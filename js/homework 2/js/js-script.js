function check (checkVar) {
    while (checkVar <= 0 || isNaN(checkVar)) {
            alert("enter your number correct ");
            checkVar = +(prompt("enter number"));
    }
    return checkVar;
}

const range = check(prompt("enter range"));
for (let i = 1; i <= range; i++) if (i%2 !== 0) console.log(i);

console.log("PART 2 , 2 NUMBERS");
let secondNumber = check(prompt("enter first number"));
let thirdNumber = check(prompt("enter second number"));
if (secondNumber > thirdNumber) [secondNumber, thirdNumber] = [thirdNumber, secondNumber];
while (secondNumber <= thirdNumber) {
     if (secondNumber%2 !== 0) console.log(secondNumber);
    secondNumber++;
}
