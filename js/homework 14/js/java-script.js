//объявляем переменные которые будут использоватья в секундомере
// Получаем через DOM Id кнопок которые будем использовать 
let secondHand = document.getElementById("secondHand");
let minuteHand = document.getElementById("minuteHand");
let stopwatch = document.getElementById("stopclock");
let start = document.getElementById("start");
let screen = document.getElementById("milisecondScreen");
start.addEventListener("click", function () {
                       clock = setInterval(function(){
                       timer.StartTimer(secondHand,minuteHand,screen,stopwatch)},10);
                       });

let stop = document.getElementById("stop");
stop.addEventListener("click", function () {
                      timer.StopTimer();
                      clearInterval(clock)
                      });

let restart = document.getElementById("restart");
restart.addEventListener("click", function () {
                      timer.RestartTimer(secondHand,minuteHand,screen,stopwatch);

        clearInterval(clock);
                      });

let timer = {
    second : 0,
    min : 0,
    pausedSecond : 0,
    pausedMin : 0,
    miliSecond : 0,
    secondArrowDeg : 0,
    MinuteArrowDeg : 0,
    miliSecond : 0,
    tempTime : 0,
    //функция которая определяет работу сеундомера
    StartTimer : function(secondHand,minuteHand,miliSecondScreen,display) {
        if (!this.tempTime) {
            this.tempTime = new Date();
        }
        let time = new Date();
        let goTime = time.getTime() - this.tempTime.getTime();
            // считаем милисикунды
        this.miliSecond = goTime - this.second*1000 - this.min*60000 + this.pausedSecond*1000 + this.pausedMin*60000;
        miliSecondScreen.innerHTML = this.miliSecond;
        console.log(this.miliSecond);
        if (timer.miliSecond >= 1000) {
            this.second++;
            this.secondArrowDeg += 6;
            secondHand.style.transform = `rotate(${this.secondArrowDeg}deg)`;

            if (this.second == 60) {
                this.min ++;
                this.second = 0;
                this.MinuteArrowDeg +=6;
                minuteHand.style.transform = `rotate(${this.secondArrowDeg}deg)`;

            }
        }
        display.innerHTML = ` 0 : ${this.min} : ${this.second}`;
    },
    StopTimer : function() {
        this.pausedSecond = this.second;
        this.pausedMin = this.min;
        this.tempTime= false;
    },
    RestartTimer : function(secondHand,minuteHand,miliSecondScreen,display) {
        this.miliSecond = this.MinuteArrowDeg = this.secondArrowDeg = this.min = this.second = this.pausedSecond = this.pausedMin = this.tempTime = 0;
        secondHand.style.transform = `rotate(${0}deg)`;
        minuteHand.style.transform = `rotate(${0}deg)`;
        miliSecondScreen.innerHTML = '000';
        display.innerHTML =`0 : 0 : 0`;
    },

}
