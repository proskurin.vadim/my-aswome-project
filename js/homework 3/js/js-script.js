function check (checkVar) {
    while (isNaN(checkVar)) {
        alert("enter your number correct ");
        checkVar = +(prompt("enter number"));
    }
    return checkVar;
}

function factorial (number) {
    let sum = 1;
    let symbol = '';
    if(number < 0) {
        number = -number;
        symbol = "-";
    }
    for (let i = 1; i <= number; i++) sum *= i;
    return symbol + sum;
}

const p = check(prompt("Enter number"));
console.log(factorial(p));