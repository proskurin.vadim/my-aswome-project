let born = prompt ("Введите свою дату в формате YYYY-MM-DD");
while (!Date.parse(born)) {
    born = prompt (`Неверная дата ${born}`)
}
//ввод рождения Юзера 
const userBirthDay = new Date (born);
//Функция расчета возраста
let getBirthDay = (date) => {
    const age = (new Date() - new Date(date))/(365*24*3600*1000);
    return Math.floor(age);
};
// Функция расчета знака зодиака
let findYourZodiac = (day,month) => {
    const zodiac =['Козерог', 'Водолей', 'Рыбы', 'Овен', 'Телец', 'Близнецы', 'Рак', 'Лев', 'Дева', 'Весы', 'Скорпион', 'Стрелец', 'Козерог'];
    const last_day =[19, 18, 20, 20, 21, 21, 22, 22, 21, 22, 21, 20, 19];
    return (day > last_day[month]) ? zodiac[month*1 + 1] : zodiac[month];
};
// Функция расчета китайского года
let findYourChinaAnimal = (year) => {
    const chinaYear = ['Крыса', 'Бык', 'Тигр', 'Кролик', 'Дракон', 'Змея', 'Лошадь', 'Коза', 'Обезьяна', 'Петух', 'Собака', 'Свинья'];
    return chinaYear[(year - 1900) % 12];
};
console.log('Зодиак',findYourZodiac(userBirthDay.getDay(),userBirthDay.getMonth()));
console.log('Китайский год',findYourChinaAnimal(userBirthDay.getFullYear()));
console.log('Возраст',getBirthDay(userBirthDay));