
let input = document.getElementsByClassName("button"),
screen = document.getElementsByClassName("display")[0].getElementsByTagName("input");
addEventListener("keypress", function(e) {
    calculator.getCalculate(screen,calculator.getChar(e));
});

for (let i = 0; i <= input.length -1; i++) {
    input[i].addEventListener("click",function() {
        calculator.getCalculate(screen,this.value)})
}

let calculator = {
    symbol : 0,
    calcValue : 0,  
    memory : 0,
    
    getChar: function (event) {
        if (event.keyCode == 13) {
            return "Enter"
        } else if (event.keyCode == 99) {
            return "C"
        } else {
            return String.fromCharCode(event.which);         
        }
    },
    
    getCalculate : function(display,number) {
        // если нажатая клавиша - число, то выводим его экран 
        if (!isNaN(number) && number != null && number != " " ) {
            display[0].value = `${display[0].value}${number}`; 
        }
        // если клавиша очистить , ощищаем  переменную и экран
        else if (number == "C") {
            display[0].value = this.calcValue = "";
        } 
        //если точка то показываем на экране число с точкой
        else if (number == ".") {            
            display[0].value = `${display[0].value}.`
        }
        //если нажат знак то число на экране присваиваем в переменную'calcValue' а знак в 'symbol' 
        else if (number =="+" || number =="-" || number =="/" || number =="*") {
            this.calcValue = display[0].value;
            display[0].value = "";
            this.symbol = number;  
           }
        // плюсуем в  'memory' , ощищаем экран
        else if (number == "m+") {
            this.memory = +this.memory + +display[0].value;
            display[0].value = ""
            }
        // минусуем  в  'memory' , ощищаем экран
        else if (number == "m-") {
            this.memory = +this.memory - +display[0].value;
            display[0].value = ""
        }
        // выводим  переменную 'memory'
        else if (number == "mrc") {
            display[0].value = `${this.memory}`;
            // если нажать еще раз то очищаем 'memory'
            if (this.memory != 0 ) {
                this.memory = 0;
            }
        }
        // если нажата равно то производим итерацию в зависимости от 'symbol'
        else if (number == "=" || number == "Enter") {

            if (this.symbol == "+" ) {
                display[0].value = (+this.calcValue) + (+display[0].value);
            }

            else if (this.symbol == "-" ) {
                display[0].value = ((+this.calcValue) - (+display[0].value)).toFixed(12)/1;
            }

            else if (this.symbol == "*" ) {
                display[0].value = (+this.calcValue) * (+display[0].value);
            }

            else if (this.symbol == "/" ) {
                display[0].value = (+this.calcValue) / (+display[0].value);
            }    
        } 
    }
    
    
}