let n = + prompt("enter Fib");
function fib(n) {
    let symbol = '',
        sum = 0,
        first = 1,
        second = 1;
    if (n <= 0){
        n = -n;
        symbol = "-";
    }
    if (n >= 0 && n <= 2) return 1;
    for (let i = 2; i < n; i++) {
        sum = first + second;
        first = second;
        second = sum;
    }
    return symbol + sum;
}
console.log(fib(n));
