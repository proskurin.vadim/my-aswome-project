function Hamburger (size, stuff) {
    var _size = size;
    var _topping = [];
    var _stuffing = stuff;
    // Function for  checking  value in  array  returns boolean-value
    var checkArg = function(value,array) {
        var flag = false;
        array.filter(function (prop) {
            if (prop.name == value) flag = true;
        });
        return flag;
    };
    // Hamburger CONST
    Hamburger.SIZE = [
        {name:"small",price:50,cal:20},
        {name:"large",price:100,cal:40}];
    Hamburger.STUF = [
        {name:"cheese", price:10, cal:20},
        {name:"salt", price:20, cal:15},
        {name:"potato", price:15, cal:10}];
    Hamburger.TOPPING = [
        {name:"mayo",price:20, cal:5},
        {name:"spice",price:20, cal:5}];
    //cher vars on correct
    try {
        if (!_size)  throw  new HamburgerException("you can't create burger without size");
        if (!_stuffing)  throw  new HamburgerException("you can't create burger without stuff");
        if (!checkArg(_size,Hamburger.SIZE)) throw  new HamburgerException("wrong size:",_size);
        _stuffing =_stuffing.split(" ");
        _stuffing.forEach(function (prop) {
            if (!checkArg(prop,Hamburger.STUF)) throw new HamburgerException("wrong stuff:",prop);
        })
    } catch(err) {
        _size = "";
        _stuffing = [];
        console.log(err.name,err.message);
    }
    //get stuff
    this.getStuff = function() {
        console.log("Ваша начинка: " + _stuffing);
        return _stuffing;
    };
    //calc price
    this.getPrice = function() {
        var price = filter ([Hamburger.STUF,Hamburger.TOPPING,Hamburger.SIZE],[_stuffing,_topping,_size],"price");
        console.log ("Ваша цена: " + price + "тугриков");
        return price;
    };
    //calc call
    this.getCal = function() {
        var cal = filter ([Hamburger.STUF,Hamburger.TOPPING,Hamburger.SIZE],[_stuffing,_topping,_size],"cal");
        console.log("Каллорийность: " + cal + "каллорий");
        return cal;
    };
    // add topping arg  - name of topping
    this.addTopping = function(value) {
        try {
            value = value.split(" ");
            value.forEach(function(arg) {
                if (!checkArg(arg,Hamburger.TOPPING)) throw  new HamburgerException("wrong topping:",arg);
                else {
                    _topping.push(arg);
                }
            })
        } catch (err) {
            console.log(err.name,err.message);
        }
    };
    // delete  arg - topping-name
    this.removeTopping = function(topping) {
        try {
            if (!_topping.remove(topping)) throw new HamburgerException("There no topping:",topping);
            else {
                _topping.remove(topping);
            }
        } catch(err) {
            console.log(err.name,err.message);
        }
    };
    //get topping
    this.getTopping = function() {
        console.log("Ваши добавки: " + _topping);
        return _topping
    };
    //get size
    this.getSize = function() {
        console.log("Вы заказали: " + _size);
        return _size
    };
    // Filter , first arg ConstArray  .
    // second UserArray , that we get after constructor .
    // third - prop that want to return (call or price)
    var filter = function(constArray,userArray,property) {
        var counter = 0;

        (function objectFilter(constArray,userArray,property) {
            if (Array.isArray(constArray)) {
                constArray.forEach(function (arg) {
                    objectFilter(arg,userArray, property);
                })
            } else {
                if (Array.isArray(userArray)) {
                    userArray.forEach(function (arg2) {
                        objectFilter(constArray, arg2, property);
                    });
                } else {
                    if (userArray == constArray.name) {
                        counter += constArray[property];
                    }
                }
            }
        }(constArray,userArray,property));
        return counter;
    };
    //Extended method 'remove' from Object
    Array.prototype.remove  = function(value) {
        if (this.indexOf(value) != -1) {
            return this.splice(this.indexOf(value),1);
        }
        return false;
    };

}
// HamburgerError
function HamburgerException (message,property) {
    this.name = "HamburgerException";
    this.message = message + property;
}
HamburgerException.prototype = SyntaxError.prototype;
var wrongBurger = new Hamburger("small","potato satlt");
var burger = new Hamburger("small","potato salt cheese");
burger.getStuff();
burger.addTopping("mayo spice");
burger.removeTopping("spicфываавфыe");
burger.getStuff();
burger.getCal();
burger.getPrice();
burger.getTopping();
burger.getSize();


