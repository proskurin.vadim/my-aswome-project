class Hamburger {
    constructor(size, ...args) {
        try {
            //check class on errors and create variables
            if (!size) throw new HamburgerError("you can't create burger without size",);
            if(!args.length) throw new HamburgerError("you can't create burger without stuff",);
            if (size.type !== "size") throw new HamburgerError("Wrong type:",size.type);
            this.size = size.name;
            this.price = size.price;
            this.cal = size.cal;
            this.stuffing = args.map((arg) => {
                if(arg.type !== "stuff") throw new HamburgerError("Wrong type:",arg.type);
                this.price += arg.price;
                this.cal += arg.cal;
                return arg.name
            });
            this.topping = [];
        } catch (err) {
            console.log(err.name, err.message);
        }
    };

    get sizeOfHamburger() {
        console.log(`Вы заказали бургер ${this.size} размерa`);
        return this.size
    };

    get stuffOfHamburger() {
        console.log(`Вы  бургер ${this.stuffing} начинками`);
        return this.stuffing
    };

    set toppingOfHamburger(newValue) {
        //check on correct topping
        try {
            if(!newValue) throw new HamburgerError("Empty topping:",);
            // if we already have newVaule in topping mas throw exept
            if(newValue.type != "topping") throw new HamburgerError("Wrong type:",newValue.type);
            this.topping.forEach((arg)=> {
                if(newValue.name === arg) throw new HamburgerError("You already add",arg);
            });
            this.price += newValue.price;
            this.cal += newValue.cal;
            this.topping.push(newValue.name);
        } catch (err) {
            console.log(err.name,err.message);
        }
    };

    get toppingOfHamburger() {
        console.log(`ваши добавки ${this.topping}`);
        return this.topping;
    };

    get hamburgerPrice() {
        console.log("ваша цена",this.price);
        return this.price ;
    }
    get hamburgerCall() {
        console.log("ваша каллорийность",this.cal);
        return this.cal;
    }
    removeTopping(value) {
        try {
            // check remove
            if (!value) throw new HamburgerError("Empty remove",);
            if (value.type != "topping") throw new HamburgerError("Wrong type:",value.type);
            this.price -= value.price;
            this.cal -= value.cal;
            // find index of value and  splice this mass
            this.topping.splice(this.topping.indexOf(value.name), 1)
        } catch (err) {
            console.log(err.name,err.message);
        }
    }
// static const for class
    static get sizeSmall() {
        return {name: "small", type:"size", price: 50, cal: 20}
    }
    static get sizeLarge() {
        return {name: "small", type:"size", price: 100, cal: 40}
    }
    static get stuffPotato() {
        return {name:"potato", type:"stuff", price: 15, cal: 10};
    }
    static get stuffCheese() {
        return {name:"cheese", type:"stuff", price: 10, cal: 20};
    }
    static get stuffSalt() {
        return  {name:"salt", type:"stuff", price: 20, cal: 15};
    }
    static get toppingMayo() {
        return {name:"mayo", type:"topping", price: 20, cal: 15};
    }
    static get toppingSpice() {
        return {name:"spice", type:"topping", price: 15, cal: 5};
    }
}
class HamburgerError extends Error {
    constructor(message,arg ="") {
        super();
        this.name = "HamburgerError";
        this.message = `${message} [${arg}]`;
    }
}
let burger = new Hamburger(Hamburger.sizeSmall,Hamburger.stuffPotato,Hamburger.stuffCheese);
burger.toppingOfHamburger = Hamburger.toppingMayo;
burger.stuffOfHamburger;
burger.sizeOfHamburger;
burger.removeTopping(Hamburger.toppingSpice);
burger.toppingOfHamburger;
burger.hamburgerPrice;
burger.hamburgerCall;
wrongBurger = new Hamburger(Hamburger.sizeSmall,);
wrongBurger.removeTopping(Hamburger.sizeSmall);