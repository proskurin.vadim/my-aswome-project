
$(function() { //Объявляем блок переменных которые будем исполбзовать
    let slider = $(".slider"),
    slideWidth = $('.slider-box').outerWidth(),
    slideCount = $('.slider__item').length,
    right = $("#right-button"),
    left = $("#left-button"),
    animateTime = 500,
    direction = 1,                  // направление по ум право
    margin = - slideWidth,
    photoNumb = 0;                     // счетчик для подсветки блока картинокВ
    $(".slider__item:last").clone().prependTo(".slider"); //последний добовляем к первому
    $(".slider__item").eq(1).clone().appendTo(".slider"); // а  первый к последнему
    $('.slider').css('margin-left', -slideWidth);

 function animate() {  //движения слайдера которое мы вызываем по клику на кнопки
    if (margin== -slideCount*slideWidth - slideWidth) {
        slider.css({'marginLeft':-slideWidth});
        margin=-slideWidth*2;
    }
    else if(margin == 0 && direction == -1) {     // если нажали  то двигаем в лево
        slider.css({'marginLeft':-slideWidth*slideCount});
        margin = -slideWidth*slideCount + slideWidth;
    } else {
        margin = margin - slideWidth *(direction);
    }
    slider.animate({'marginLeft':margin},animateTime);
 }
left.click(function(e) {
    photoNumb--; // проходим по блоку картинок и  меняем цвет
    if (photoNumb < 0) {
        photoNumb = 3;
    }
    //меняем стили нижней панельки
    $(`.people__image`).css('bottom',`0`);
    $(`.people__image`).eq(photoNumb).css('bottom',`18px`);
    $(`.people__image`).css('border-color',`rgba(31, 218, 181, 0.2)`)
    $(`.people__image`).eq(photoNumb).css('border-color',`rgba(30, 195, 168, 0.7)`);


    direction = -1;
    animate();
    e.preventDefault();
  });
  right.click(function(e) {
    photoNumb++ ;
      if (photoNumb > 3) {
        photoNumb = 0;
    }
    //меняем стили нижней панельки
    $(`.people__image`).css('bottom',`0`);
    $(`.people__image`).eq(photoNumb).css('bottom',`18px`);
    $(`.people__image`).css('border-color',`rgba(31, 218, 181, 0.2)`);
    $(`.people__image`).eq(photoNumb).css('border-color',`rgba(30, 195, 168, 0.7)`);

    direction = 1;
    animate();
    e.preventDefault();
  });

})
