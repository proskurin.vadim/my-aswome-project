let $textBlock = $('<div class="works-description"><a href="#" class="chain"><i class="fas fa-link works-icons"></i></a><a href="#" class="look"><i class="fas fa-search fa-search-green"></i></a><h3 class="works-header">Creative design</h3><p class="works-text">graphic design</p></div>');
let $preloader = $(`<div class="preloader"><div class="page-loader-circle"></div></div>`);
$(`.work-presentation-img`).slice(12).hide();//прячем лишнее
//при наведении показываем текст блок
$('.work-presentation-img').hover( function() {
   $(this).append($textBlock);
}, function() {
    $textBlock.remove();
})
// клик на кнопку Load More
$("#works-loader").click(function(e) {
    $(`body`).append($preloader); // прелоадер
    setTimeout(function() {
        $preloader.remove();
    }, 2000); // через 2 секунды убрать
    $('.work-presentation-img:hidden').slice(0,12).show(); //показываем скрытые по 12
    if ($('.work-presentation-img:hidden').length == 0) {  //если скрытых нет удаляем
        $("#works-loader").remove();
    }
    e.preventDefault();
})
$(".js-works-menu__link").click(function(e){   //клик на кнопки фильтра
    e.preventDefault();
    const category = $(this).data("info");  // получить категорию
    $(".work-presentation-img").hide();
    $(`.work-presentation-img[data-info="${category}"]`).slice(0, 12).show();    // отфилтровать категорию
    if ($(this).data("info") == "all" ) {
        $(".work-presentation-img").show();
        $("#works-loader").remove();
    }
})
