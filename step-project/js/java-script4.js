jQuery(window).load(MasonryTest);  // при загрузки сразу сортируем картинки
function MasonryTest() {  //функция  сортировки мансори блока
	jQuery('.mansory-images-block').masonry({
      columnWidth: 370,
      itemSelector: '.mansory-image-item',
      gutter: 18
    });
};

$('.mansory-image-item').slice(16).hide();//прячем лишнее
//при наведении показываем текст блок
$("#best-img-loader").click(function(e) {
    $(`body`).append($preloader); // прелоадер
    setTimeout(function() {
        $preloader.remove();
    }, 2000); // через 2 секунды убрать
    $('.mansory-image-item:hidden').slice(0,12).show(); //показываем скрытые по 12
    if ($('.mansory-image-item:hidden').length == 0) {  //если скрытых нет удаляем
        $("#best-img-loader").remove();
    }
    e.preventDefault();
});
$("#best-img-loader").click(MasonryTest); // при клике на кнопку вновь обновляем блок мансори
